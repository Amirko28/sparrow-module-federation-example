import React from "react";

const SparrowTitle = ({text = "Hello World"}) => {
    return <h1>Sparrow says: {text}</h1>
}

const SparrowText = ({text = "im text"}) => {
    return <p>Sparrow says: {text}</p>
}

export default {SparrowText, SparrowTitle};