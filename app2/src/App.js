import React, { useState, useEffect } from 'react';

// app1 - Exports SparrowComponents
// app3 - Exports MiriComponents
// app2 - Imports both

export default () => {
  const [Modules, setModules] = useState([]);

  useEffect(() => {
    const fetch = async () => {
      const sparrowMods = await import('app1/SparrowComponents');
      const miriMods = await import('app3/MiriComponents')
      const temp = Modules;
      Object.keys(sparrowMods.default).forEach((key, index) => {
        temp.push({key: key, component: sparrowMods.default[key]});
      });
      Object.keys(miriMods.default).forEach((key, index) => {
        temp.push({key: key, component: miriMods.default[key]});
      });
      setModules([...temp]);
    };
    fetch();
  }, []);

  return (
    <>
      {
        Modules.map(mod => 
          React.createElement(
            mod.component,
            { 
              key: mod.key,
              text: "yes",
              title: "bla"
            }
          )
        )
      }
    </>
  )
};
