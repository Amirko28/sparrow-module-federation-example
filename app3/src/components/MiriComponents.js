import React from "react";

const MiriTitle = ({text = "miri title"}) => {
    return <h1 style={{color: "red"}}>Sparrow says: {text}</h1>
}

const MiriText = ({text = "miri text"}) => {
    return <p style={{color: "red"}}>Sparrow says: {text}</p>
}

export default {MiriTitle, MiriText};